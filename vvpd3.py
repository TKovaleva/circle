'''Программа выводит сумму площадей красных областей (ввпд 13 вариант 3 практическая)'''
import math
import unittest


class GetTest(unittest.TestCase):
    def test_get(self):
        self.assertEqual(get_non_negative_float([2, 4]), [2, 4])

    def test_get2(self):
        self.assertEqual(get_non_negative_float([1, 1]), [1, 1])

    def test_get3(self):
        self.assertEqual(get_non_negative_float([-2, 4, 5]), False)

    def test_get4(self):
        self.assertEqual(get_non_negative_float([1, 'a', 5]), False)

    def test_get5(self):
        self.assertEqual(get_non_negative_float([1, 0]), False)


class RedBlueTest(unittest.TestCase):
    def test_red_blue1(self):
        self.assertEqual(red_and_blue_square([2, 4, 5]), (40.840704496667314, 37.69911184307752, 78.53981633974483))

    def test_red_blue2(self):
        self.assertEqual(red_and_blue_square([1, 2, 3, 4]), (18.84955592153876, 9.42477796076938, 28.274333882308138))

    def test_red_blue3(self):
        self.assertEqual(red_and_blue_square([2, 2]), (12.566370614359172, 0.0, 12.566370614359172))


class ProbabilityTest(unittest.TestCase):
    def test_prob1(self):
        self.assertEqual(probability(40.840704496667314, 37.69911184307752, 78.53981633974483), (0.52, 0.48))

    def test_prob2(self):
        self.assertEqual(probability(40, 37, 100), (0.4, 0.37))


def get_non_negative_float(list_rad):
    f = True

    for i in range(len(list_rad)):
        try:
            list_rad[i] = float(list_rad[i])
            if list_rad[i] <= 0:
                f = False
                raise ValueError
            else:
                continue
        except ValueError:
            f = False
            break

    if f:
        return list_rad
    else:
        return False


def red_and_blue_square(list_rad):
    list_rad = set(list_rad)
    list_rad = sorted(list_rad)
    summ_red = math.pi * (math.pow(list_rad[0], 2))
    stp = len(list_rad)
    if stp % 2 != 1:
        stp = len(list_rad) - 1

    for i in range(1, stp, 2):
        print(i)
        summ_red += math.pi * (math.pow(list_rad[i + 1], 2) - math.pow(list_rad[i], 2))

    summ_all = math.pi * (math.pow(list_rad[stp - 1], 2))
    summ_blue = summ_all - summ_red
    return summ_red, summ_blue, summ_all


def probability(summ_red, summ_blue, summ_all):
    pr = summ_all / 100
    p_red = (summ_red / pr) / 100
    p_blue = (summ_blue / pr) / 100
    return p_red, p_blue


def main():
    list_rad = input().split()
    get_non_negative_float(list_rad)

    if get_non_negative_float(list_rad):
        summ_red, summ_blue, summ_all = red_and_blue_square(list_rad)
        print(red_and_blue_square(list_rad))
        p_red, p_blue = probability(summ_red, summ_blue, summ_all)
        print(probability(summ_red, summ_blue, summ_all))
        print("Суммарная площадь красных областей - ", round(summ_red, 4))
        print("Суммарная площадь синих областей - ", round(summ_blue, 4))
        print("Вероятность попасть в красную область", round(p_red, 2))
        print("Вероятность попасть в синюю область", round(p_blue, 2))
    else:
        print("Введите положительные числа через пробел")


if __name__ == '__main__':
    main()
